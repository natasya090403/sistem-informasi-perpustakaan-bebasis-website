<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Daftar Buku</title>
	<link rel="stylesheet" type="text/css" href="style buku.css">
</head>
<body>
	<div class="container">
		<main class="grid">
			<article>
				<img src="pp.jpg" width="200px" height="200px">
				<div class="konten">
					<h2>Buku Pelajaran</h2>
					<p>Berisi buku mata pelajaran.</p>
				</div>
			</article>
			<article>
				<img src="nn.jpg" width="200px" height="200px">
				<div class="konten">
					<h2>Buku Novel</h2>
					<p>Berisi buku novel</p>
				</div>
			</article>
			<article>
				<img src="jj.jpg" width="200px" height="200px">
				<div class="konten">
					<h2>Buku Komik</h2>
					<p>Berisi buku komik.</p>
				</div>
			</article>
			<article>
				<img src="crpn.jpg" width="200px" height="200px">
				<div class="konten">
					<h2>Buku Cerpen</h2>
					<p>Berisi buku cerita-cerita pendek</p>
				</div>
			</article>
			<article>
				<img src="fiksi.jpg" width="200px" height="200px">
				<div class="konten">
					<h2>Buku Fiksi</h2>
					<p>Berisi buku fiksi-fiksi</p>
				</div>
			</article>
			<article>
				<img src="nonfiksi.jpg" width="200px" height="200px">
				<div class="konten">
					<h2>Buku Non Fiksi</h2>
					<p>Berisi buku non fiksi didalamnya</p>
				</div>
			</article>
			<article>
				<img src="nn.jpg" width="200px" height="200px">
				<div class="konten">
					<h2>Kamus</h2>
					<p>berisikan kamus-kamus dimulai dari kamus bahasa inggris, bahasa indonesia dll</p>
				</div>
			</article>
			<article>
				<img src="mjlh.jpg" width="200px" height="200px">
				<div class="konten">
					<h2>Majalah</h2>
					<p>berisikan majalah-majalah</p>
				</div>
			</article>
		</main>
	</div>
</body>
</html>