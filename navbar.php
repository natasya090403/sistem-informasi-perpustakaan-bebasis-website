<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Navbar</title>
	<link rel="stylesheet" href="style nav.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
</head>
<body>
	<nav>
		<input type="checkbox" id="check">
		<label for="check">
			 <i class="material-icons" id="btn">subject</i>
			 <i class="material-icons" id="close">close</i>
		</label>
		<h2>PERPUSKU</h2>
		<ul>
			<li><a class="active" href="#">Home</a></li>
			<li><a href="buku.php">Daftar Buku</a></li>
			<li><a href="#">Daftar Anggota</a></li>
			<li><a href="about.php">About</a></li>
			<li><a href="logout.php">Logout</a></li>
		</ul>
	</nav>

</body>
</html>